using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SsdWebApi.Models;

namespace SsdWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IndiceController : ControllerBase
    {
        private readonly IndiceContext _context;
        private Persistence P;

        public IndiceController(IndiceContext context)
        { 
            _context = context;
            P = new Persistence(context);
        }
        [HttpGet]
        public ActionResult<List<Indice>> GetAll() => _context.indici.ToList();
        
        [HttpGet("{attribute}", Name = "GetSerie")]
        public string GetSerie(string attribute)
        {
            string res = "{";
            Forecast F = new Forecast();
            res += F.forecastARIMAindex(attribute);
            res += "}";

            Console.WriteLine(res);

            //var index = P.readIndex(attribute);
            return res;
        }
    /*
        // GET by ID action
        [HttpGet("{id}")]
        public async Task<ActionResult<Indice>> GetIndice(int id)
        { 
            var indiceItem = await _context.indici.FindAsync(id);
            if (indiceItem == null)
            { 
                return NotFound();
            }
            return indiceItem;
        }

        // POST action
        [HttpPost]
        [Route("[action]")]
        public string PostIndiceItem([FromBody] Indice item)
        {
            string res="Data "+item.Data;
            try
            { 
                _context.indici.Add(item);
                _context.SaveChangesAsync();
            }
            catch(Exception ex)
            { 
                Console.WriteLine("[ERROR] "+ex.Message);
                res = "Error";
            }
            Console.WriteLine(res);
            return res;
        }

        // PUT action
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIndice(int id,[FromBody] Indice item)
        {
            if (id != item.id) return BadRequest();
            _context.Entry(item).State = EntityState.Modified;
            try
            { 
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (!_context.indici.Any(s => s.id == id))
                { 
                    return NotFound();
                }
                else
                { 
                    Console.WriteLine("[ERROR] "+ex.Message);;
                }
            }
            return Ok();
        }

        // DELETE action
        [HttpDelete("{id}")]
        public async Task<ActionResult<Indice>> DeleteIndice(int id)
        {
            var item = await _context.indici.FindAsync(id);
            if (item == null)
            { 
                return NotFound();
            }
            _context.indici.Remove(item);
            await _context.SaveChangesAsync();
            return item;
        }
    */
    }
} 